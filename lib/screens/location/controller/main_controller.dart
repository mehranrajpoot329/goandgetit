import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:go_on_get_it/constants/routes.dart';
import 'package:go_on_get_it/data/local/my_hive.dart';
import 'package:go_on_get_it/data/local/user_location.dart';
import 'package:go_on_get_it/network/remote_repository.dart';
import 'package:go_on_get_it/widgets/allow_location.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';

class MainController extends GetxController {
  RxInt tappedIndex = 0.obs;
  RxInt catalogIndex = 0.obs;
  var isDropdownOpened = false.obs;
  bool isCatalogDropDown = false;
  RxString itemSelected = "10 Km".obs;
  RxString seletedItem = "allNearMe".tr.obs;
  OverlayEntry? radiusLay;
  RxString selectedCat = 'catalog'.tr.obs;
  OverlayEntry? catalogLay;
  RxBool isThroughShops = false.obs;
  var isLoading = false.obs;
  RxList categories = [].obs;
  RxList popular = [].obs;
  RxList latest = [].obs;
  RxList popularOffers = [].obs;
  RxString locationAddress = "No Location".obs;
  RxBool isFirstTime = false.obs;
  late GoogleMapController mapController;
  final Completer<GoogleMapController> completerController = Completer();
  final GoogleMapsPlaces places =
      GoogleMapsPlaces(apiKey: 'AIzaSyD0ETkUaNH0X0k7Yyjpa_6_o0KnS0sA90o');
  TextEditingController emailController = TextEditingController();
  TextEditingController reportController = TextEditingController();
  RxBool isLocationFetchedLoading = false.obs;

  @override
  void onInit() async {
    super.onInit();
    if (MyHive.getLocation() != null && MyHive.getToken() != null) {
      locationAddress.value = await findAddress(MyHive.getLocation());
      getCategories();
      getShops(MyHive.getLocation());
    } else {
      Future.delayed(100.milliseconds, () {
        Get.dialog(const AllowLocation(), barrierDismissible: false);
      });
    }
  }

  void navigate() async {
    var result = await Get.toNamed(Routes.locationScreen);
    updateLocation(result);
  }

  @override
  void disposeId(Object id) {
    super.disposeId(id);
    emailController.dispose();
    reportController.dispose();
  }

  @override
  void onReady() {
    print("onReady");
  }

  Future<void> userAuth(UserLocation type, String deviceId) async {
    isLoading.value = true;
    Map<String, dynamic> loginMap = {
      'uid': deviceId,
      'lat': type.latitude,
      'lng': type.longitude,
    };
    isFirstTime.value = await RemoteRepository.userAuth(loginMap);
    if (!isFirstTime.value && Get.isDialogOpen != null) {
      Get.back();
      getCategories();
      getShops(MyHive.getLocation());
    }
  }

  void getCategories({int radius = 10}) async {
    isLoading.value = true;
    var location = MyHive.getLocation();
    Map<String, dynamic> categoriesMap = {
      'lat': location.latitude,
      'lng': location.longitude,
      'radius': radius,
    };
    categories.value = await RemoteRepository.getCategories(categoriesMap) ?? [];
    isLoading.value = false;
  }

  Future<String?> _getId() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  showSearch(bool isShow) {
    isThroughShops.value = isShow;
  }

  @override
  void dispose() {
    mapController.dispose();
    super.dispose();
  }

  ///initial Position
  final CameraPosition kGooglePlex = const CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  ///current Location
  Future<void> determinePosition() async {
    isLocationFetchedLoading.value = true;
    LocationPermission permission;
    permission = await Geolocator.checkPermission();
    log('Permission status : $permission');
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    Position position = await getCurrentLocation();
    MyHive.setLocation(UserLocation(longitude: position.longitude, latitude: position.latitude));
    locationAddress.value = await findAddress(MyHive.getLocation());
    isLocationFetchedLoading.value = false;
    //if user first time
    // if (MyHive.getToken() == null) {
    //   var deviceId = await _getId() ?? '';
    //   userAuth(MyHive.getLocation(), deviceId);
    // }
  }

  void isDiscountOffer() async {
    if (MyHive.getToken() == null) {
      var deviceId = await _getId() ?? '';
      userAuth(MyHive.getLocation(), deviceId);
    }
    Get.back();
  }

  Future<Position> getCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    return position;
  }

  void updateLocation(result) async {
    //if user first time from map....
    locationAddress.value = await findAddress(MyHive.getLocation());
    if (MyHive.getToken() == null) {
      var deviceId = await _getId() ?? '';
      userAuth(result, deviceId);
    } else {
      getCategories();
      getShops(MyHive.getLocation());
    }
  }

  Future<String> findAddress(UserLocation type) async {
    var placeMarkers = await placemarkFromCoordinates(type.latitude, type.longitude);
    var completeAddress = '${placeMarkers.first.street},${placeMarkers.first.locality},${placeMarkers.first.country}';
    return completeAddress;
  }

  void getShops(location) async {
    Map<String, dynamic> shopMap = {
      'lat': location.latitude,
      'lng': location.longitude,
    };

    var value = await RemoteRepository.getShops(shopMap);

    List _latestList = [];
    List _popularList = [];

    latest.clear();
    for (var item in value.latest) {
      _latestList.add(item);
    }

    popular.clear();
    for (var item in value.popular) {
      _popularList.add(item);
    }

    if(MyHive.getExpiredDiscount() == false){
      for(int i=0; i<_latestList.length; i++){
        _latestList[i].offers!.removeWhere((offer) => offer.isExpired == 1);
        latest.add(_latestList[i]);

        _popularList[i].offers!.removeWhere((offer) => offer.isExpired == 1);
        popular.add(_popularList[i]);
      }
      latest.removeWhere((shop) => shop.offers!.isEmpty);
      popular.removeWhere((shop) => shop.offers!.isEmpty);
    }
    else{
      _latestList.removeWhere((shop) => shop.offers!.isEmpty);
      latest.addAll(_latestList);

      _popularList.removeWhere((shop) => shop.offers!.isEmpty);
      popular.addAll(_popularList);
    }




    // latest.clear();
    // for (var item in value.latest) {
    //   latest.add(item);
    // }
    //
    // popular.clear();
    // for (var item in value.popular) {
    //   popular.add(item);
    // }
  }

  ///post report api
  void postReport() async {
    setLoading(true);
    Map<String, dynamic> reportMap = {
      "email": emailController.value.text,
      "description": reportController.value.text
    };
    await RemoteRepository.postReport(reportMap);
    setLoading(false);
    Get.back();
    emailController.clear();
    reportController.clear();
  }

  ///loader.....
  void setLoading(bool isLoading) {
    if (isLoading && !Get.isDialogOpen!) {
      Get.defaultDialog(
        title: "",
        content: const CircularProgressIndicator(strokeWidth: 4),
      );
    } else if (Get.isDialogOpen!) {
      Get.back();
    }
  }

  void contact(Map<String, dynamic> formData) async {
    setLoading(true);
    await RemoteRepository.contactUs(formData);
    setLoading(false);
  }
}
