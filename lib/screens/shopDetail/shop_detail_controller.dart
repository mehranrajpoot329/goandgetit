import 'dart:developer';

import 'package:get/get.dart';
import 'package:go_on_get_it/data/local/my_hive.dart';
import 'package:go_on_get_it/models/classes/shop.dart';
import 'package:go_on_get_it/network/remote_repository.dart';

class SubSubCategoryController extends GetxController {
  List<ShopDataRows> shopList = <ShopDataRows>[].obs;
  List<ShopDataRows> shopFilterList = <ShopDataRows>[].obs;
  RxBool isLoading = false.obs;
  RxBool isSearching = false.obs;
  RxBool isNoItems = false.obs;

  // void setList() {
  //   shopList.clear();
  //   shopList.addAll(dummyData);
  // }

  @override
  void onInit() {
    super.onInit();
    getShops();
  }

  Future<void> getShops({String? filter, String? categoryId, bool showLoading = true}) async {
    isNoItems.value = false;
    if(showLoading) {
      isLoading.value = true;
    } else{
      isLoading.value = false;
    }
    var location = MyHive.getLocation();
    Map<String, dynamic> queryParams = {
      'filter': filter ?? 'catalog',
      'category_id': categoryId,
      'radius': 5000,
      // 'item': 'test',
      // 'shop': 'kfc',
      'lat': location.latitude,
      'lng': location.longitude,
      'limit': -1,
      'offset': 0
    };
    Future.delayed(500.milliseconds, () async {
      try {
        if(shopList.isNotEmpty){
          shopList.clear();
        }
        List<ShopDataRows> list = <ShopDataRows>[];
        list = (await RemoteRepository.fetchShopsList(queryParams)) ?.cast<ShopDataRows>() ?? [];

        if(MyHive.getExpiredDiscount() == false){
        for(int i=0; i<list.length; i++){
              list[i].offers!.removeWhere((offer) => offer.isExpired == 1);
              shopList.add(list[i]);
          }
        shopList.removeWhere((shop) => shop.offers!.isEmpty);
        }
        else{
          list.removeWhere((shop) => shop.offers!.isEmpty);
          shopList.addAll(list);
        }
      } finally {
        isLoading.value = false;
        if(shopList.isEmpty){
          isNoItems.value = true;
        }
        print('Get shops Reached');
      }
    });
  }

  Future<void> getSearchedShops({String? filter, String? searchedTitle}) async {
    isNoItems.value = false;
    isLoading.value = true;
    var location = MyHive.getLocation();
    Map<String, dynamic> queryParams = {
      'filter': filter,
      // 'category_id': categoryId,
      // 'radius': 5000,
      'item': searchedTitle,
      'shop': searchedTitle,
      'lat': location.latitude,
      'lng': location.longitude,
      'limit': -1,
      'offset': 0
    };
    Future.delayed(500.milliseconds, () async {
      try {
        if(shopList.isNotEmpty) {
          shopList.clear();
        }
        List<ShopDataRows> _searchedList = <ShopDataRows>[];
        _searchedList = (await RemoteRepository.fetchShopsList(queryParams)) ?.cast<ShopDataRows>() ?? [];

        if(MyHive.getExpiredDiscount() == false){
          for(int i=0; i<_searchedList.length; i++){
            _searchedList[i].offers!.removeWhere((offer) => offer.isExpired == 1);
          }
          _searchedList.removeWhere((shop) => shop.offers!.isEmpty);
          shopList = _searchedList.toSet().toList();
        }
        else{
          _searchedList.removeWhere((shop) => shop.offers!.isEmpty);
          shopList = _searchedList.toSet().toList();
          // shopList.addAll(_searchedList);
        }
      }
      finally {
        isLoading.value = false;
        if(shopList.isEmpty){
          isNoItems.value = true;
        }
        print('Get Searched shops Reached');
      }
    });
  }

  Future<void> addShopClick(int shopID) async {
    Map<String, dynamic> queryParams = {
      'shop_id': shopID,
    };
    try {
      var response = await RemoteRepository.addShopClick(queryParams);
      if (!response['error']) {
        log(response['message']);
      }
    } catch (e) {
      log('Exception in AddShopClick : $e');
    }
  }

  Future<void> addOfferClick(int offerID) async {
    Map<String, dynamic> queryParams = {
      'offer_id': offerID,
    };
    try {
      var response = await RemoteRepository.addOfferClick(queryParams);
      if (!response['error']) {
        log(response['message']);
      }
    } catch (e) {
      log('Exception in addOfferClick : $e');
    }
  }

  void getShopsFilter(String filterString) {
    if (filterString.isNotEmpty) {
      shopFilterList = shopList
          .where((element) =>
              element.name!.toLowerCase().contains(filterString.toLowerCase()))
          .toList();
      log(shopFilterList.length.toString());
    }
  }
}
