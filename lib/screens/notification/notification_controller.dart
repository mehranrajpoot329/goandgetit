import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:go_on_get_it/models/classes/notification_list_data.dart';
import 'package:go_on_get_it/network/remote_repository.dart';

class NotificationController extends GetxController{

  List<NotificationListData> notificationList = <NotificationListData>[];
  
  int limit = 10;
  int offset = 0;
  var type = 'new';
  var isLoading = false.obs;



  Future<void> fetchNotificationData() async {
    isLoading.value = true;
      Map<String, dynamic> queryParams = {
        'limit': limit,
        'offset': offset,
        'text': type
      };
        Future.delayed(500.milliseconds, () async {
          try{
            notificationList = await RemoteRepository.fetchNotificationList(queryParams)??[];
            if (kDebugMode) print(notificationList);
          }
          finally{
          isLoading.value = false;
          update();
          }
        });
  }
}