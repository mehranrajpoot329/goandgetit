import 'dart:developer';

import 'package:get/get.dart';
import 'package:go_on_get_it/data/local/my_hive.dart';
import 'package:go_on_get_it/models/classes/favorite/favorite.dart';
import 'package:go_on_get_it/network/remote_repository.dart';

class FavoriteController extends GetxController {
  List<FavoriteDataRows>? favoriteList = <FavoriteDataRows>[].obs;
  List<FavoriteDataRows> favoriteSearchedList = <FavoriteDataRows>[].obs;
  RxBool isNoSearchItems = false.obs;
  RxBool isFav = true.obs;
  var text = 'op';
  int limit = 10;
  int offset = 0;
  var isLoading = false.obs;

  @override
  void onInit() {
    fetchFavoriteListData(MyHive.getLocation());
    super.onInit();
  }

  Future<void> fetchFavoriteListData(location, {bool showLoading = true}) async {
    if(showLoading) {
      isLoading.value = true;
    } else{
      isLoading.value = false;
    }
    Map<String, dynamic> queryParams = {
      'lat': location.latitude,
      'lng': location.longitude,
    };
    Future.delayed(500.milliseconds, () async {
      try {
        favoriteList = (await RemoteRepository.fetchFavoriteList(queryParams))?.cast<FavoriteDataRows>() ?? [];
      } finally {
        isLoading.value = false;
        print('Reached');
      }
    });
  }

  void fetchFavoriteSearch({required String value, Function()? updateList}){
    isNoSearchItems.value = false;
    favoriteSearchedList.clear();
    // favoriteSearchedList.addAll(favoriteList!);
    if(value.length > 1){
      for (var favItem in favoriteList!) {
        if(favItem.name!.toLowerCase().contains(value.toLowerCase())){
          favoriteSearchedList.add(favItem);
        }
      }
      updateList!();
      if(favoriteSearchedList.isEmpty){
        isNoSearchItems.value = true;
      }
    }else{
      favoriteSearchedList.clear();
    }
  }

  Future<bool> favoriteToggle(int shopId) async {
    Map<String, dynamic> formData = {
      'shop_id': shopId,
    };
    dynamic response = await RemoteRepository.toggleLike(formData);
    if (!response['error']) {
      log(response.toString());
      return true;
    } else {
      return false;
    }
  }

  Future<bool> favoriteShopDetailToggle(int shopId) async {
    Map<String, dynamic> formData = {
      'shop_id': shopId,
    };
    dynamic response = await RemoteRepository.toggleLike(formData);
    if (response['message'] == 'Shop unliked successfully') {
      log(response.toString());
      return false;
    } else {
      return true;
    }
  }

  void notificationIsSeen(int? id) async {
    Map<String, dynamic> formData = {
      'notification_id':id,
    };
    await RemoteRepository.setNotificationSeen(formData);
  }


}
