

import 'notification_data.dart';

class NotificationResp {
  bool? error;
  String? message;
  NotificationData? data;

  NotificationResp({this.error, this.message, this.data});

  NotificationResp.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    data = json['data'] != null ? NotificationData.fromJson(json['data']) : null;
  }
}