import 'package:go_on_get_it/models/classes/shop_owner.dart';

class NotificationListData {
  int? id;
  String? title;
  String? body;
  String? createdAt;
  String? updatedAt;
  int? shopId;
  int? isSeen;
  ShopOwner? shopOwner;

  NotificationListData(
      {this.id,
      this.title,
      this.body,
      this.createdAt,
      this.updatedAt,
      this.shopId,
      this.isSeen,
      this.shopOwner,
      });

  NotificationListData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    body = json['body'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    shopId = json['shop_id'];
    isSeen = json['is_seen'];
    shopOwner = json['ShopOwner'] != null
        ? ShopOwner.fromJson(json['ShopOwner'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = {};
    data['id'] = id;
    data['title'] = title;
    data['body'] = body;
    data['createdAt'] = createdAt;
    data['updatedAt'] = updatedAt;
    data['shop_id'] = shopId;
    data['is_seen'] = isSeen;
    if (shopOwner != null) {
      data['ShopOwner'] = shopOwner!.toJson();
    }
    return data;
  }
}