import 'notification_list_data.dart';

class NotificationData {
  int? count;
  List<NotificationListData>? rows;

  NotificationData({this.count, this.rows});

  NotificationData.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    if (json['rows'] != null) {
      rows = <NotificationListData>[];
      json['rows'].forEach((v) {
        rows!.add(NotificationListData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['count'] = this.count;
    if (this.rows != null) {
      data['rows'] = this.rows!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}